class Desafio
	def initialize 
		@max_linhas = 6
		@max_colunas = 7
		@fumaca = 'X'
	end

  def valida_mapa(mapa)
    (!mapa[0][@max_linhas].nil? && !mapa[@max_linhas][@max_colunas].nil?)
  end

  def conta_aeroportos(mapa)
    count = 0

		0.upto(@max_linhas) do |i|
			0.upto(@max_colunas) do |j|
				count += 1 if mapa[i][j] == "A"
			end
		end

		count
	end

	def expande(mapa)
		expansao = []

		mapa.each_with_index do |linha, x|
			linha.each_with_index do |val, y|
				if val == @fumaca
					x_minus_1 = x - 1
					x_plus_1 = x + 1
					y_minus_1 = y - 1
					y_plus_1 = y + 1
					expansao << [x_minus_1, y] unless x_minus_1 < 0
					expansao << [x_plus_1, y] unless x_plus_1 > @max_colunas
					expansao << [x, y_minus_1] unless y_minus_1 < 0
					expansao << [x, y_plus_1] unless y_plus_1 > @max_linhas
				end
			end
		end

		expansao.each do |x, y|
			mapa[x][y] = @fumaca
		end

		mapa
	end
end
