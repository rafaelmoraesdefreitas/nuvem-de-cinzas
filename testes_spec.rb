require 'rspec'
load 'desafio.rb'

describe 'ValidaMapa' do

  it 'Verifica se o mapa tem 7 linhas e 8 colunas' do
    mapa = [[" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "]]

    ds = Desafio.new()
    ds.valida_mapa(mapa).should == true
  end
end

describe 'ContagemAeroportos0' do

  it 'Retorna 0 aeroportos' do
    mapa = [[" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "]]

    ds = Desafio.new()
    ds.conta_aeroportos(mapa).should == 0
  end
end

describe 'ContagemAeroportos2' do

  it 'Retorna 2 aeroportos' do
    mapa = [[" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", "A", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", "A"]]

    ds = Desafio.new()
    ds.conta_aeroportos(mapa).should == 2
  end
end

describe 'ContagemAeroportosTodos' do

  it 'Retorna 56 aeroportos' do
    mapa = [["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"],
            ["A", "A", "A", "A", "A", "A", "A", "A"]]

    ds = Desafio.new()
    ds.conta_aeroportos(mapa).should == 56
  end
end

describe 'TestesReplicaMapa' do

  it 'Retorna a copia do mapa como resultado' do
    mapa = [[" ", " ", "X", " ", " ", " ", "X", "X"],
            [" ", "X", "X", " ", " ", " ", " ", " "],
            ["X", "X", "X", " ", "A", " ", " ", "A"],
            [" ", "X", " ", " ", " ", " ", " ", " "],
            [" ", "X", " ", " ", " ", " ", "A", " "],
            [" ", " ", " ", "A", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "]]



    ds = Desafio.new()
    ds.expande(mapa).should == mapa
  end
end

describe 'TesteExpansaoDia1' do

  it 'Retorna situacao dia 1' do
    mapa = [[" ", " ", "X", " ", " ", " ", "X", "X"],
            [" ", "X", "X", " ", " ", " ", " ", " "],
            ["X", "X", "X", " ", "A", " ", " ", "A"],
            [" ", "X", " ", " ", " ", " ", " ", " "],
            [" ", "X", " ", " ", " ", " ", "A", " "],
            [" ", " ", " ", "A", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "]]

esperado = [[" ", "X", "X", "X", " ", "X", "X", "X"],
            ["X", "X", "X", "X", " ", " ", "X", "X"],
            ["X", "X", "X", "X", "A", " ", " ", "A"],
            ["X", "X", "X", " ", " ", " ", " ", " "],
            ["X", "X", "X", " ", " ", " ", "A", " "],
            [" ", "X", " ", "A", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " "]]

    ds = Desafio.new()
    ds.expande(mapa).should == esperado
  end
end
